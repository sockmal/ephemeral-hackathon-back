///// DECLARE CONST /////
/////////////////////////
const requestJson = require('request-json'); // Import request-json Library
const baseUrlApiBBVA = "https://ei-gateway.bbvaportalapis.es/serviceVirtualization/ASO/";
const paginationView = "paginationKey=0&pageSize=200"
const financialManagementView = "transactions/V01/financialManagementView"
// Import the Google Cloud client library
const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery(); 

// Retorna un número aleatorio entre min (incluido) y max (excluido)
function getRandomArbitrary() {
  return Math.floor(Math.random() * 10);
}

function foreach(arr, func){
  for(var i in arr){
    func(i, arr[i]);
  }
}

////// FUNCTIONS //////
///////////////////////
// Get Debt Stadistics from Big Query
function getDebtStadisticBigQueryV1(req, res) {
  console.log("GET /ephemeral/v1/DebtStadisticBigQuery");
  
  var DebtElectricQuery = 'select * from datos.estadisticas';
  bigquery.query(DebtElectricQuery).then(function(data) {
    console.log("Getting ContractIdS from bigquery");
    var rowsresponse = data[0];
    res.send(rowsresponse);
  });
}

// Get Transactions by ContractIds
function postAccountTransactionV1(req, res) {

  console.log("POST /ephemeral/v1/AccountTransaction");  
  
  // Example to filter categories
  // POST application/JSON
  // {"contractIds":["ESXXX"],"dateRange":{"from":"2019-03-07T00:00:00.000+0200","to":"2019-08-06T13:31:00.085+0200"},"humanCategories":["9999"],"humanSubcategories":["9999"]}
  var customerIds = req.body
  var httpClient = requestJson.createClient(baseUrlApiBBVA);
  
  // Control the response status
  httpClient.post(financialManagementView + "?" + paginationView, customerIds,
    function(err, resAPIMarket, body) {

      if (err) {
        response = body;
        console.log("ERROR getting Transactions by ContractIds");
        res.status(500);
      } else {
        var responseData = {}
        transactionArray = body;
        var jsonAmountArray = [];
        foreach(transactionArray.cardTransactions, function(i, data){
          var tdate = data.transactionDate.slice(0,10);
          var tamount = data.amount.amount - getRandomArbitrary();
          var transactionJson = {
            "date": tdate,
            "amount": tamount
          }
          jsonAmountArray.push(transactionJson);
          
        });
        jsonAmountArray.sort(function (a, b) {
          if (a.date > b.date) {
            return 1;
          }
          if (a.date < b.date) {
            return -1;
          }
          // a must be equal to b
          return 0;
        });
        console.log("sending body in the response");
        response = jsonAmountArray;
      }
      res.send(response);
    }
)
}


////// MODULE EXPORTS ///////
/////////////////////////////

// Get Transactions by ContractIds from ASO
module.exports.postAccountTransactionV1 = postAccountTransactionV1;

// Get Transactions by ContractIds from BigQuery
module.exports.getDebtStadisticBigQueryV1 = getDebtStadisticBigQueryV1;