///// DECLARE CONST /////
/////////////////////////
const requestJson = require('request-json'); // Import request-json Library
const baseUrlApiBBVA = "https://ei-gateway.bbvaportalapis.es/serviceVirtualization/ASO/";
const contextualDataV2 = "contextualData/V02/"

////// FUNCTIONS //////
///////////////////////

// Consulta de path
function getHealthCheck(req, res) {
  console.log("GET /ephemeral/v1/healthcheck")
  res.status(200);
  console.log(process.argv[2]);
  response = "HEALTHCHECK STATUS: IS OK!"
  res.send(response);
}

// Get Customer
function getCustomerByIdV1(req, res) {

  console.log("GET /ephemeral/v1/customer/:customerid");
  
  var httpClient = requestJson.createClient(baseUrlApiBBVA);

  var customerID = String(req.params.customerid);
  console.log("Function getCustomerByIdV1 - The account customerID is -> " + customerID);
  
  // Control the response status
  httpClient.get(contextualDataV2 + customerID,
    function(err, resAPIMarket, body) {
      if (err) {
        response = body;
        console.log("ERROR getting customerID");
        res.status(500);
      } else {
        console.log("sending body in the response");
        response = body;
      }
      res.send(response);
    }
)
}


////// MODULE EXPORTS ///////
/////////////////////////////

// Get Customer V1
module.exports.getHealthCheck = getHealthCheck;

// Get Customer V1
module.exports.getCustomerByIdV1 = getCustomerByIdV1;