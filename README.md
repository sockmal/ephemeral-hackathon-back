# ephemeral-hackathon-back - NodeJS API Project
![Node API](/images/nodeApiLogo.png)

## Description
This project provides a Node API.

## API METHODS

## Init project and Install Express web application framework for Node.js
Express.js, or simply Express, is a web application framework for Node.js, released as free and open-source software under the MIT License. It is designed for building web applications and APIs. It has been called the de facto standard server framework for Node.js

```bash
npm init
npm install express --save
```

## Install Testing Libraries and others
```
npm install nodemon --save-dev
npm install request-json --save
```

## Start Node API locally with nodemon
Nodemon restart the service automatically when detect changes in the filesystem

```
npm start
```

## Start Node API locally with node
This option is better than others because give you more info when there are problems.
```
node server.js
```

## Commands to build and run node API with docker in your localhost

### Build Image
```
docker build -t ephemeral-hackathon-back .
```

### Run Image
```
docker run -d -p 8080:8080 ephemeral-hackathon-back
```
Test your rest api with postman or other tools.

#### Tag and push image to dockerhub
```
docker tag ephemeral-hackathon-back yourdockerhubuser/ephemeral-hackathon-back
docker push yourdockerhubuser/ephemeral-hackathon-back
```

## Example to test SNAPSHOT IMAGE
```
docker run -d -p 8080:8080 sockmal/ephemeral-hackathon-back:1.0.0-SNAPSHOT
```