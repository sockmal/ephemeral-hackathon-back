# DOCKER NODE IMAGE BASE
FROM node:8.15-jessie-slim

# BIGQUERY CONFIG
ENV GOOGLE_APPLICATION_CREDENTIALS="/project/bigquery.json"

# WORKING DIRECTORY
WORKDIR /project

# COPY PROJECT
ADD . /project

# INSTALL DEPENDENCIES
RUN npm install --only=prod

# Expose node port
EXPOSE 8080

# CMD INIT
CMD ["node", "server.js"]