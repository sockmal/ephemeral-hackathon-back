///// IMPORT & INIT EXPRESS FRAMEWORK /////
///////////////////////////////////////////
require('dotenv').config(); //Import dotenv Library to use environment variables
const express = require('express'); // Import express Framework
const app = express(); //Init express


///// SERVER PROPERTIES //////
//////////////////////////////
const port = process.env.PORT || 8080; // Get server Port from env PORT or use default port 8080.
app.listen(port); // This allow start the server listening in port 8080.
console.log("API are listening in port... " + port);


///// ENABLE EXPRESS JSON /////
//////////////////////////////
app.use(express.json()); // Add preprocessor that it provides to get the body as json, if not it arrives as undefined.


///// DEFINE CONTROLLERS /////
//////////////////////////////
const CustomerController = require('./controllers/CustomerController');
const AccountTransaction = require('./controllers/AccountTransaction');


///// CORS OPTIONS TO ENABLED IT /////
//////////////////////////////////////
var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

  // This will be needed.
  res.set("Access-Control-Allow-Headers", "Content-Type, authorization");

  next();
 }

app.use(enableCORS); // Enabled to use CORS


///// DEFINE API METHODS/////
/////////////////////////////

// Get path api
app.get("/ephemeral/v1/healthcheck", CustomerController.getHealthCheck);

// Get Customer by ID V1
app.get("/ephemeral/v1/customer/:customerIds", CustomerController.getCustomerByIdV1);

// Get Transactions by ContractIds
app.post("/ephemeral/v1/AccountTransaction", AccountTransaction.postAccountTransactionV1);

// Get Debt Stadistics from Big Query
app.get("/ephemeral/v1/DebtStadisticBigQuery", AccountTransaction.getDebtStadisticBigQueryV1);